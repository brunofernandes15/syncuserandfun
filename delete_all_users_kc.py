from keycloak import KeycloakAdmin
from keycloak.exceptions import raise_error_from_response, KeycloakGetError
import logging

#from keycloak import *




keycloak_admin = KeycloakAdmin(server_url="http://localhost/auth/",
                               username='admin',
                               password='admin',
                               realm_name="master",
                               client_id='admin-cli',
                               verify=True)

def get_creds(user_id):
    GET_USER_CREDS= "admin/realms/{realm-name}/users/{id}/credentials"
    params_path = {"realm-name": keycloak_admin.realm_name, "id": user_id}
    data_raw = keycloak_admin.raw_get(GET_USER_CREDS.format(**params_path))
    return raise_error_from_response(data_raw, KeycloakGetError)

def delete_creds(user_id, creds_id):
    DELETE_CREDS_URL = "admin/realms/{realm-name}/users/{id}/credentials/{creds-id}"
    params_path = {"realm-name": keycloak_admin.realm_name, "creds-id": creds_id, "id": user_id}
    data_raw = keycloak_admin.raw_delete(DELETE_CREDS_URL.format(**params_path))
    return raise_error_from_response(data_raw, KeycloakGetError)

realms = keycloak_admin.get_realms()

keycloak_admin.realm_name = "cmporto";
keycloak_admin.auto_refresh_token = ["get"];

clients = keycloak_admin.get_clients()

users = [];

users = keycloak_admin.get_users({})

# filtered_users = list(filter(lambda item: "username" in item and item["username"] in users_list , users))

#User = keycloak_admin.get_user(users[0]["id"])
for user in users:
    keycloak_admin.delete_user(user["id"])

