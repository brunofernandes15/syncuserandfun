from pymongo import MongoClient
import xlrd
import requests
from keycloak import KeycloakOpenID
import json
import time
import logging

default_pw = "airc##00"
mongo_host = "localhost"
URL = "https://srvlsadsgctestm01.ad.airc.pt"


username_admin = 'admin'
password_admin = 'oSiIwHOfDMQ6b9M0V3Bj0Y8'
realm_admin = 'admin-cli'
client_id = 'sad'
client_username = 'sad.airc@airc.pt'
client_password = 'sad.airc'
realm_name = 'sad-tes1'


client = MongoClient('mongodb://' + mongo_host + ':27017')
db = client.userstocreate
data = db.data

for row in data.find():
    # print(str(row))

    # Ler as informações do funcionário
    num_meca = str(row['meca'])
    nome = str(row['nome'])
    email = str(row['email'])

    # Ir buscar token de KeycloakAdmin
    data = {"username": username_admin, "password": password_admin,
            "client_id": realm_admin,
            "grant_type": "password"}
    r = requests.post(URL + "/auth/realms/master/protocol/openid-connect/token", headers={'Content-Type': 'application/x-www-form-urlencoded'},
                      data=data,
                      verify=False
                      )
    # print(r)
    access_token = json.loads(r.text)["access_token"]

    # Criar utilizador
    data = {"username": email, "email": email,
            "firstName": nome, "enabled": "true"}
    r = requests.post(URL + "/auth/admin/realms/"+realm_name+"/users", data=json.dumps(data),
                      headers={'Content-Type': 'application/json',
                               'Authorization': 'Bearer {}'.format(access_token)},
                      verify=False)

    # Ir buscar id de utilizador
    r = requests.get(URL + "/auth/admin/realms/"+realm_name+"/users?search=" + email,
                     headers={'Content-Type': 'application/json',
                              'Authorization': 'Bearer {}'.format(access_token)},
                     verify=False)
    id_utilizador = r.json()[0]["id"]

    # Meter palavra pass não temporária
    data = {"type": "password", "value": default_pw, "temporary": "false"}
    r = requests.put(URL + "/auth/admin/realms/"+realm_name+"/users/" + id_utilizador + "/reset-password", data=json.dumps(data),
                     headers={'Content-Type': 'application/json',
                              'Authorization': 'Bearer {}'.format(access_token)},
                     verify=False)

    # print(id_utilizador)

    # Meter palavra pass não temporária
    # 021ef731-980b-49d6-873c-ee873b70d1f3
    # uuidFoto

    # data = {"email": email, "firstName": nome, "attributes": {"uuidFoto": ["49a0b911-4193-4566-aad1-9f6efe6d52bb"]} }
    # r = requests.put(URL + "/auth/admin/realms/"+realm_name+"/users/" + id_utilizador, data = json.dumps(data),
    #     headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})

    # print(r)

    # Fazer login client para criar ir buscar um token
    keycloak_openid = KeycloakOpenID(server_url=URL + "/auth/",
                                     client_id="sad",
                                     realm_name=realm_name,
                                     client_secret_key="",
                                     verify=False)
    token = keycloak_openid.token(client_username, client_password)
    access_token = token["access_token"]
    # print(access_token)
    # Ir buscar o UUID de funcionario ao FUN
    r = requests.get(URL + "/api/v1/fun/funcionario?numMeca=" + num_meca,
                     headers={'Content-Type': 'application/json',
                              'Authorization': 'Bearer {}'.format(access_token)},
                     verify=False)
    # print(r)
    if (r.json()["totalElements"] > 0):
        funcionario = r.json()["content"][0]
        # print(funcionario)
    else:
        logging.warning('Não encontrado no FUN:' + num_meca + ' ' + nome)
    print(funcionario)
    # Ir editar o uuid de utilizador do funcionario ao FUN
    r = requests.put(URL + "/api/v1/fun/funcionario/" + funcionario["id"] + "/id-utilizador/" + id_utilizador,
                     headers={'Content-Type': 'application/json',
                              'Authorization': 'Bearer {}'.format(access_token)},
                     verify=False)
    # print(json.dumps(id_utilizador))
    # print(r)
    logging.info(funcionario["entidade"]["nome"] + "--" + email)
