from time import process_time
import datetime
import requests
from pymongo import MongoClient



def participant_aproved_and_on_date(nickname, endDate, beginingDate, authors):

    return filter(lambda a:  a["user"]["participated_on"] != None and
                  a["user"]["nickname"] == nickname and
                  endDate >= a["user"]["participated_on"] and
                  beginingDate <= a["user"]["participated_on"], authors)


auth_cookie = '_WSK=6.66349671258; optintowebauth=1; csrftoken=OqzVY3lQoWW40ESUZ35yqNtO9A3k8gm3l4lCtQtAGAcDmPyCwuaLnZmV7ulrRed2; _ga=GA1.2.971988747.1599822439; ajs_anonymous_id="f253741e-eed6-44a7-a17e-4af13d9d201b"; ajs_group_id=null; ajs_anonymous_id="f253741e-eed6-44a7-a17e-4af13d9d201b"; wordpress_google_apps_login=795b2cb1f934e9a78aa966327032346c; ajs_group_id=null; __stripe_mid=0f4aaee9-9339-4365-8102-c6fc7fa2f35eca7c78; atlCohort={"bucketAll":{"bucketedAtUTC":"2022-03-11T15:03:34.026Z","version":"2","index":23,"bucketId":0}}; atlUserHash=1393648548; atl_xid.ts=1647011014132; atl_xid.current=[{"type":"xc","value":"ce8528fb-e6d2-48d2-a922-1ffaf4dcc502","createdAt":"2020-10-23T14:43:42.210Z"}]; OptanonAlertBoxClosed=2022-03-11T15:03:35.413Z; OptanonConsent=landingPath=NotLandingPage&datestamp=Fri+Mar+11+2022+15:03:59+GMT+0000+(Western+European+Standard+Time)&version=4.6.0&EU=true&groups=1:1,2:1,3:1,4:1,0_147330:1,0_155512:1,0_173487:1,0_173483:1,0_145889:1,0_173485:1,0_146517:1,0_144595:1,0_155516:1,0_155514:1,0_172277:1,0_155513:1,0_173486:1,0_162898:1,0_145888:1,0_173482:1,0_173484:1,0_144594:1,0_150453:1,0_173480:1,0_155515:1,101:1,117:1,120:1,144:1,160:1,180:1&AwaitingReconsent=false; _gid=GA1.2.67936287.1650556267; cloud.session.token=eyJraWQiOiJzZXNzaW9uLXNlcnZpY2VcL3Byb2QtMTU5Mjg1ODM5NCIsImFsZyI6IlJTMjU2In0.eyJhc3NvY2lhdGlvbnMiOltdLCJzdWIiOiI1YzZkMmM0ZmVmYzk2ODYyOTNhYmMyNzEiLCJlbWFpbERvbWFpbiI6ImFpcmMucHQiLCJpbXBlcnNvbmF0aW9uIjpbXSwiY3JlYXRlZCI6MTY0NjkzMTMyOSwicmVmcmVzaFRpbWVvdXQiOjE2NTA2NDEwMjMsInZlcmlmaWVkIjp0cnVlLCJpc3MiOiJzZXNzaW9uLXNlcnZpY2UiLCJzZXNzaW9uSWQiOiJkMThmZjdlNC0yMTQzLTRmY2ItYTgwZi0zZjIzN2ZiNTE2NTkiLCJzdGVwVXBzIjpbXSwiYXVkIjoiYXRsYXNzaWFuIiwibmJmIjoxNjUwNjQwNDIzLCJleHAiOjE2NTMyMzI0MjMsImlhdCI6MTY1MDY0MDQyMywiZW1haWwiOiJqb2FvLnJvZHJpZ3Vlc0BhaXJjLnB0IiwianRpIjoiZDE4ZmY3ZTQtMjE0My00ZmNiLWE4MGYtM2YyMzdmYjUxNjU5In0.O8k4OTVHjFB1a_Y1Yf3kIY4ARxi8N9Fr5gr9D6Gm9hO6NV2MTQfEvO8ySV8Kn9RTTUlqiodWT_HXBSpqWrIWs5I-jqsKRfCpVQPjbCPeyIgfiEl7Z8ot8aS36-PwVzNoT1-ePa2-flL2ykamQFzmtMuZbkzQBg-9NWJoASiqS1EHaQLRn3Ngg5iWEHoS8RNiuKV461ztNSFxB1uVUsdx9BbuBTHx6Thq6cc7ti6z8PnLvTsZ5YAI2WJtJnqKmH-Q9Tvk3_gNyi3aI9KoDMdSPeexKWLT6Ebcuwkv3vFI-SgYgvLLeiWROU_fjaAh5Z-XobDqV1Z003NtxaE5gza_BA'
reviwer = "João Rodrigues"
benginingDate = "2021-12-01"
endDate = "2021-12-31"

mongo_host = "localhost"

client = MongoClient('mongodb://' + mongo_host + ':27017')
db = client.prs
data = db.data
data.delete_many({})

applications = [
    "sad",
    "old",
    "fun",
    "adm",
    "instalacoes-koi",
    "ansible-role-koi",
    "koi-libraries"
]
prs = list()

for app in applications:
    r = requests.get("https://bitbucket.org/!api/2.0/repositories/airc/" + app + "/pullrequests?pagelen=50&fields=%2Bvalues.participants&q=state%3D%22MERGED%22&page=1",
                     headers={'cookie': auth_cookie})
                    #  "https://bitbucket.org/!api/2.0/repositories/airc/sad/pullrequests?pagelen=25&fields=%2Bvalues.participants&q=state%3D%22MERGED%22&page=1"
    if r.ok:
        prs = prs + r.json()["values"]

data.insert_many(prs);
#data.update_many({}, {'$set': {'updated_on_date': {'$toDate':'$updated_on'}}});


#data.aggregate([{'$project': {'updated_on_date': { '$convert': { 'to': 'date', 'input': '$updated_on' } }}}])

# db.prs.updateMany({},[ {$set: {updated_on: {$toDate:"$updated_on"}}}])



  {$and: [
      {
          "participants.user.nickname": {$regex: "João Rodrigues"
          }
      },
      {
          "participants.approved": {$eq: true
          }
      },{
          "updated_on": {
              "$gte":new Date("2022-03-01"),
              $lte: new Date("2022-03-31")
              }
          }
    
    
  ]
  }