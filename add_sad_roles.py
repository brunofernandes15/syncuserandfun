import xlrd
import requests
from keycloak import KeycloakOpenID
from keycloak import KeycloakAdmin
import json
import time
import logging
import csv
start = time.time()
# URL = "http://srvltestcmporto02.ad.airc.pt"
URL = "http://srvltestcmporto.ad.airc.pt"
username_admin = 'admin'
password_admin = 'admin'
realm_admin = 'admin-cli'
client_id = 'sad'
client_username = 'sad.airc@airc.pt'
client_password = 'sad.airc'
realm_name = 'sad-test-1'

# Abrir o Excel
# loc = ("./fun_porto.xlsx") 
# wb = xlrd.open_workbook(loc) 
# sheet = wb.sheet_by_index(0) 


# Log para um ficheiro, atenção que o ficheiro acumula sempre,
# não limpa cada vez que o script é executado
# logging.basicConfig(filename='logs_create_users_fun.log',level=logging.DEBUG)

# Dar check do status do FUN
# Dar check do status do keycloak

with open("./usersr.csv", "rt") as f:
    reader = csv.reader(f, delimiter=",")
    for i, line in enumerate(reader):

        # print('line[{}] = {}'.format(i, line))
        # print(line[0])
        email = line[0]
        #teste login 
        data = {"username": email, "password": line[1],
                "client_id": 'sad', 
                "grant_type": "password"}
        r = requests.post(URL + "/auth/realms/sad-test-1/protocol/openid-connect/token", headers={'Content-Type': 'application/x-www-form-urlencoded'},
            data = data)
        # print(r)
        # print(r.ok)
        if not r.ok:
            print('badpw: '+ email)

            #Ir buscar token de KeycloakAdmin
            data = {"username": username_admin, "password": password_admin,
                    "client_id": realm_admin, 
                    "grant_type": "password"}
            r = requests.post(URL + "/auth/realms/master/protocol/openid-connect/token", headers={'Content-Type': 'application/x-www-form-urlencoded'},
                data = data)
            print('admin token')
            print(r)
            access_token = json.loads(r.text)["access_token"]
                #Ir buscar id de utilizador     
            r = requests.get(URL + "/auth/admin/realms/"+realm_name+"/users?search=" + email, 
                headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})
            id_utilizador = r.json()[0]["id"]


            #Meter palavra pass não temporária
            data = {"type": "password", "value": line[1], "temporary": "false"}
            r = requests.put(URL + "/auth/admin/realms/"+realm_name+"/users/" + id_utilizador + "/reset-password", data = json.dumps(data), 
                headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})
            print('changed pw')
            print(r)       
        # access_token = json.loads(r.text)["access_token"]

        # r = requests.get(URL + "/auth/admin/realms/"+realm_name+"/users?search=" + email, 
        # headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})
        # id_utilizador = r.json()[0]["id"]
        
        
# for row in range(1, sheet.nrows):
#     print(str(row) + "/" + str(sheet.nrows))

#     #Ler as informações do funcionário
#     num_meca = str(int(sheet.cell_value(row, 0)))
#     nome = sheet.cell_value(row, 1)
#     username = sheet.cell_value(row, 2)
#     email = sheet.cell_value(row, 3)

        
#      #Ir buscar token de KeycloakAdmin
#     data = {"username": username_admin, "password": password_admin,
#             "client_id": realm_admin, 
#             "grant_type": "password"}
#     r = requests.post(URL + "/auth/realms/master/protocol/openid-connect/token", headers={'Content-Type': 'application/x-www-form-urlencoded'},
#         data = data)
#     #print(r)
#     access_token = json.loads(r.text)["access_token"]

#     #Criar utilizador
#     data = {"username": email, "email": email, "firstName": nome, "enabled": "true"}
#     r = requests.post(URL + "/auth/admin/realms/"+realm_name+"/users", data = json.dumps(data), 
#         headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})

#     #Ir buscar id de utilizador     
#     r = requests.get(URL + "/auth/admin/realms/"+realm_name+"/users?search=" + email, 
#         headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})
#     id_utilizador = r.json()[0]["id"]


#     #Meter palavra pass não temporária
#     data = {"type": "password", "value": username, "temporary": "false"}
#     r = requests.put(URL + "/auth/admin/realms/"+realm_name+"/users/" + id_utilizador + "/reset-password", data = json.dumps(data), 
#         headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})

#     #print(id_utilizador)

#     #Meter palavra pass não temporária
#         #021ef731-980b-49d6-873c-ee873b70d1f3
#     #uuidFoto
#     data = {"email": email, "firstName": nome, "attributes": {"uuidFoto": ["49a0b911-4193-4566-aad1-9f6efe6d52bb"]} }
#     r = requests.put(URL + "/auth/admin/realms/"+realm_name+"/users/" + id_utilizador, data = json.dumps(data), 
#         headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})

#     #print(r)

#     #Fazer login client para criar ir buscar um token
#     keycloak_openid = KeycloakOpenID(server_url=URL + "/auth/",
#                         client_id="sad",
#                         realm_name = realm_name,
#                         client_secret_key="")
#     token = keycloak_openid.token(client_username, client_password)
#     access_token = token["access_token"]
#     #print(access_token)
#     # Ir buscar o UUID de funcionario ao FUN
#     r = requests.get(URL + "/api/v1/fun/funcionario?numMeca=" + num_meca, 
#                     headers={'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})

#     if (r.json()["totalElements"] > 0):
#         funcionario = r.json()["content"][0]
#         #print(funcionario)
#     else:
#         logging.warning('Não encontrado no FUN:' + num_meca + ' ' + nome)

#     # Ir editar o uuid de utilizador do funcionario ao FUN
#     r = requests.put(URL + "/api/v1/fun/funcionario/" + funcionario["id"] + "/id-utilizador/" + id_utilizador,
#                     headers = {'Content-Type':'application/json', 'Authorization': 'Bearer {}'.format(access_token)})
#     #print(json.dumps(id_utilizador))
#     print(r)
#     print(funcionario["entidade"]["nome"] + "--" + username)

# print("--- Demorou %s segundos" % (time.time() - start))
