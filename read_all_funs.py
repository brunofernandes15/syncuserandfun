import psycopg2
from pymongo import MongoClient
import unidecode

email_domain = "@websad-test.airc.pt"
mongo_host = "localhost"

postgres_host = "localhost"
postgres_user = "postgres"
postgres_password = "postgres"

realm_name = 'sad-devs'
tenant_id = "2"
ciclos= "12,11"
try:
    client = MongoClient('mongodb://' + mongo_host + ':27017')
    db = client.userstocreate

    data = db.data
    data.delete_many({})
    # //-----------------SAD
    connectionSAD = psycopg2.connect(user=postgres_user,
                                     password=postgres_password,
                                     host=postgres_host,
                                     port="5432",
                                     database="sad")

    cursorSAD = connectionSAD.cursor()
    # Print PostgreSQL Connection properties
    print(connectionSAD.get_dsn_parameters(), "\n")

    # Print PostgreSQL version
    cursorSAD.execute(
        "select distinct funcionario_uuid FROM public.sad_avaliado WHERE tenant_id=" + tenant_id + " and tipo = 'NORMAL' and  apagado=false and ciclo_id in (" + ciclos +")")
    rowAvalaidos = cursorSAD.fetchall()

    cursorSAD.execute(
        "select distinct funcionario_uuid FROM public.sad_avaliador WHERE tenant_id=" + tenant_id + " and apagado=false and ciclo_de_avaliacao_id in (" + ciclos +")")
    rowAvalaidores = cursorSAD.fetchall()

    for elem in rowAvalaidos:
        # print(elem[0])
        repeated = data.find_one({'funcionario': elem[0]})
        if repeated is None:
            data.insert_one({
                'funcionario': elem[0],
                'type': ['avaliado']
            })
        else:
            data.update_one({'funcionario': elem[0]}, {
                            '$push': {'type': "avaliado"}})
    for elem in rowAvalaidores:
        # print(elem[0])
        repeated = data.find_one({'funcionario': elem[0]})
        if repeated is None:
            data.insert_one({
                'funcionario': elem[0],
                'type': ['avaliador']
            })
        else:
            data.update_one({'funcionario': elem[0]}, {
                            '$push': {'type': "avaliador"}})
    # //-----------------FUN

    connectionFUN = psycopg2.connect(user=postgres_user,
                                     password=postgres_password,
                                     host=postgres_host,
                                     port="5432",
                                     database="fun")

    cursorFUN = connectionFUN.cursor()
    # Print PostgreSQL Connection properties
    print(connectionFUN.get_dsn_parameters(), "\n")

    # Print PostgreSQL version
    cursorFUN.execute(
        "select fun.uuid, ent.nome, fun.num_meca FROM public.fun_funcionario fun, fun_entidade ent where ent.uuid = fun.entidade and fun.tenant_id=" + tenant_id)
    funcionarios = cursorFUN.fetchall()
    # print(funcionarios)
    # print(len(funcionarios))
    # print(funcionarios)
    for fun in funcionarios:
        myquery = {"funcionario": fun[0]}
        current = data.find_one(myquery)
        email = unidecode.unidecode(fun[1].split()[0]).lower(
        ) + '.' + unidecode.unidecode(str(fun[2])).lower()
        emailquery = {"email": email + email_domain}
        i = 1
        while data.count_documents(emailquery) > 0:
            email = unidecode.unidecode(fun[1].split()[0]).lower()
            for x in range(-i, 0, 1):
                email = email + '.' + \
                    unidecode.unidecode(fun[1].split()[x]).lower()
            email = email + '.' + unidecode.unidecode(str(fun[2])).lower()
            emailquery = {"email": email + email_domain}
            i += 1
        # print(email)
        email = email + email_domain
        newvalues = {"$set": {"nome": fun[1], "meca": fun[2],
                              "email": email}}
        print(current)
        if current is None:
            data.insert_one({"nome": fun[1], "meca": fun[2],
                             "email": email})
        else:
            data.update_one(myquery, newvalues)


except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    # closing database connection.
    if(connectionSAD):
        cursorSAD.close()
        connectionSAD.close()
        print("PostgreSQL connection is closed")
    if(connectionFUN):
        cursorFUN.close()
        connectionFUN.close()
        print("PostgreSQL connection is closed")
