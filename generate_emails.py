import psycopg2
import unidecode
from pymongo import MongoClient
client = MongoClient()
try:
    connectionENT = psycopg2.connect(user = "postgres",
                                  password = "postgres",
                                  host = "192.168.50.15",
                                  port = "5432",
                                  database = "ent")

    cursor = connectionENT.cursor()
    # Print PostgreSQL Connection properties
    print ( connectionENT.get_dsn_parameters(),"\n")

    # Print PostgreSQL version
    cursor.execute("select uuid, nome, id FROM public.entidade")
    entidades = cursor.fetchall()
    
    # print(funcionarios)

    client = MongoClient('mongodb://localhost:27017')
    db = client.userstocreate

    data = db.data

    for ent in entidades:
        myquery = { "entidade": ent[0] }
        current = data.find_one(myquery)

        newvalues = { "$set": { "nome": ent[1], "email": unidecode.unidecode(ent[1].partition(' ')[0]).lower() + '.' + str(666 if current is None else current["meca"]) + "@onefakedomain.pt"  } }
        data.update_one(myquery, newvalues)
        
except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)
finally:
    #closing database connection.
        if(connectionENT):
            cursor.close()
            connectionENT.close()
            print("PostgreSQL connection is closed")   