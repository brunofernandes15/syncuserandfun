import psycopg2
from pymongo import MongoClient
client = MongoClient()
try:
    connectionFUN = psycopg2.connect(user = "postgres",
                                  password = "postgres",
                                  host = "192.168.50.15",
                                  port = "5432",
                                  database = "fun")

    cursor = connectionFUN.cursor()
    # Print PostgreSQL Connection properties
    print ( connectionFUN.get_dsn_parameters(),"\n")

    # Print PostgreSQL version
    cursor.execute("select uuid, entidade, num_meca FROM public.fun_funcionario")
    funcionarios = cursor.fetchall()
    
    # print(funcionarios)

    client = MongoClient('mongodb://localhost:27017')
    db = client.userstocreate

    data = db.data

    for fun in funcionarios:
        myquery = { "funcionario": fun[0] }
        newvalues = { "$set": { "entidade": fun[1], "meca": fun[2] } }
        data.update_one(myquery, newvalues)

except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)
finally:
    #closing database connection.
        if(connectionFUN):
            cursor.close()
            connectionFUN.close()
            print("PostgreSQL connection is closed")   