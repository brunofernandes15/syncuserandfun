from keycloak import KeycloakAdmin
from keycloak.exceptions import raise_error_from_response, KeycloakGetError
#from keycloak import *




keycloak_admin = KeycloakAdmin(server_url="http://websad.cm-porto.net/auth/",
                               username='admin',
                               password='********',
                               realm_name="master",
                               client_id='admin-cli',
                               verify=True)

def get_creds(user_id):
    GET_USER_CREDS= "admin/realms/{realm-name}/users/{id}/credentials"
    params_path = {"realm-name": keycloak_admin.realm_name, "id": user_id}
    data_raw = keycloak_admin.raw_get(GET_USER_CREDS.format(**params_path))
    return raise_error_from_response(data_raw, KeycloakGetError)

def delete_creds(user_id, creds_id):
    DELETE_CREDS_URL = "admin/realms/{realm-name}/users/{id}/credentials/{creds-id}"
    params_path = {"realm-name": keycloak_admin.realm_name, "creds-id": creds_id, "id": user_id}
    data_raw = keycloak_admin.raw_delete(DELETE_CREDS_URL.format(**params_path))
    return raise_error_from_response(data_raw, KeycloakGetError)

realms = keycloak_admin.get_realms()

keycloak_admin.realm_name = "cmporto";

clients = keycloak_admin.get_clients()

users = [];

users = keycloak_admin.get_users({})

users_list =["bertasalgueiro",
"jeronimorocha",
"mariavale",
"nestorpereira",
"lidiaaraujo",
"oscarsilva",
"claudiacunha",
"sandramatos",
"carlamesquita",
"marinaloureiro",
"alziragomes",
"paulomota",
"celiacarvalho",
"carvalhosousa",
"herminiaduarte",
"fernandofonseca",
"carmocosta",
"rosacunha",
"naterciaramos",
"moreirapinto",
"emiliamagano",
"pedromeireles",
"joaquimgomes",
"vitorinosousa",
"rosamaria",
"manuelspereira",
"reiscosta",
"miguelpsousa",
"candidapereira",
"nunomorais",
"manuelmoura",
"teresacorreia",
"brunobranco",
"vascocoelho",
"conceicaolima",
"josemarques",
"capelamartins",
"antonioquarteu",
"silvaantonio",
"ruicarvalho",
"franciscoambrosio",
"marconogueira",
"carloslima",
"alfredosilva",
"fernandogoncalves",
"manuelmaia",
"andreiaferreira",
"mariafbarbosa",
"amandiocarneiro",
"evaoliveira",
"sergiovieira"]

filtered_users = list(filter(lambda item: "username" in item and item["username"] in users_list , users))

#User = keycloak_admin.get_user(users[0]["id"])
for user in filtered_users:

    creds = get_creds(user["id"])

    for cred in list(filter(lambda item: "type" in item and item["type"]=="password" , creds)):
        delete_creds(user["id"], cred["id"])

    user['requiredActions'] = [];

    keycloak_admin.update_user(user_id = user["id"], payload = user)
    # keycloak_admin.
